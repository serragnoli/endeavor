package endeavor.model

sealed trait Verification

case object Verified extends Verification

case object ElapsedTimeUnreasonable extends Verification

case object TeamsPointsUnreasonable extends Verification