package endeavor.model

import scala.concurrent.duration.Duration
import scala.concurrent.duration.Duration.Zero

final case class Threshold(action: Duration, teamPoints: Int)

object Threshold {
  def empty = Threshold(Zero, 0)
}