package endeavor.model

import scala.collection.immutable.SortedSet

final case class Game(uuid: String, threshold: Threshold, events: SortedSet[GameEvent] = SortedSet.empty[GameEvent])

object Game {
  def empty: Game = Game("", Threshold.empty, SortedSet.empty[GameEvent])
}

