package endeavor.model

import scala.concurrent.duration.Duration

final case class GameEvent(unused: Int, elapsedTime: Duration, totalPointsTeam1: Int, totalPointsTeam2: Int, whoScored: Team,
                     pointsScored: Int, verification: Verification = Verified) extends Ordered[GameEvent] {
  override def compare(that: GameEvent): Int = this.elapsedTime compare that.elapsedTime
}

object GameEvent {
  def empty = GameEvent(0, Duration.Zero, 0, 0, Team1, 0, Verified)
}
