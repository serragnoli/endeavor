package endeavor.model

final case class EventSettings(targetLength: Int, paddingText: String)
