package endeavor.model

sealed trait Team

case object Team1 extends Team

case object Team2 extends Team

object Team {
  def apply(flag: Int): Team = if (flag == 0) Team1 else Team2
}


