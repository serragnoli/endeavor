package endeavor.parser

import endeavor.model.{EventSettings, GameEvent, Team}
import scala.concurrent.duration._
import scala.language.postfixOps

import scala.annotation.tailrec

object EventRawDataParser {
  def padLeft: ((String, EventSettings)) => String = tup => {
    val (eventRep, evtSettings) = tup

    val paddingChars = evtSettings.paddingText * evtSettings.targetLength

    paddingChars.drop(eventRep.length) + eventRep
  }

  def chopUnused: String => (Int, String) = eventRepr => (eventRepr.head.asDigit, eventRepr.tail)

  def chopElapsedTime: ((Int, String)) => (Int, Int, String) = tup => {
    val (unused, tailEventRepr) = tup
    (unused, convertToBase10(tailEventRepr.take(12).reverse), tailEventRepr.drop(12))
  }

  def chopTotalPointTeam1: ((Int, Int, String)) => (Int, Int, Int, String) = tup => {
    val (unused, elapsedTime, tailEventRepr) = tup
    (unused, elapsedTime, convertToBase10(tailEventRepr.take(8).reverse), tailEventRepr.drop(8))
  }

  def chopTotalPointTeam2: ((Int, Int, Int, String)) => (Int, Int, Int, Int, String) = tup => {
    val (unused, elapsedTime, totalPointsTeam1, tailEventRepr) = tup
    (unused, elapsedTime, totalPointsTeam1, convertToBase10(tailEventRepr.take(8).reverse), tailEventRepr.drop(8))
  }

  def chopWhoScored: ((Int, Int, Int, Int, String)) => (Int, Int, Int, Int, Team, String) = tup => {
    val (unused, elapsedTime, totalPointsTeam1, totalPointsTeam2, tailEventRepr) = tup
    (unused, elapsedTime, totalPointsTeam1, totalPointsTeam2, Team(tailEventRepr.take(1).toInt), tailEventRepr.drop(1))
  }

  def chopPointsScored: ((Int, Int, Int, Int, Team, String)) => (Int, Int, Int, Int, Team, Int) = tup => {
    val (unused, elapsedTime, totalPointsTeam1, totalPointsTeam2, whoScored, tailEventRepr) = tup
    (unused, elapsedTime, totalPointsTeam1, totalPointsTeam2, whoScored, convertToBase10(tailEventRepr.take(2).reverse))
  }

  def createEvent: ((Int, Int, Int, Int, Team, Int)) => Either[String, GameEvent] = tup => {
    val (unused, elapsedTime, totalPointsTeam1, totalPointsTeam2, whoScored, pointsScored) = tup
    Right(GameEvent(unused, elapsedTime seconds, totalPointsTeam1, totalPointsTeam2, whoScored, pointsScored))
  }

  @tailrec
  private def convertToBase10(binary: String, currExp: Int = 1, currValue: Int = 0): Int = {
    binary match {
      case "" => currValue
      case b => convertToBase10(b.tail, currExp * 2, currValue + (b.head.asDigit * currExp))
    }
  }
}
