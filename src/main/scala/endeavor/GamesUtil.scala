package endeavor

import endeavor.model.{EventSettings, Game, GameEvent}
import endeavor.parser.EventRawDataParser._
import endeavor.validation.GameEventsValidation._

import scala.collection.immutable.SortedSet
import scala.util.Try

object GamesUtil {
  def parseEvent(eventRep: Int)(implicit eventSettings: EventSettings): Either[String, GameEvent] = {
    padLeft andThen
      chopUnused andThen
      chopElapsedTime andThen
      chopTotalPointTeam1 andThen
      chopTotalPointTeam2 andThen
      chopWhoScored andThen
      chopPointsScored andThen
      createEvent apply ((eventRep.toBinaryString, eventSettings))
  }

  def addToGame(event: GameEvent, game: Game): Either[String, Game] = {
    for {
      _ <- validateNoPointsScored(event)
      _ <- validateDuplicated(event, game)
      et <- flagSuspiciousElapsedTime(event, game)
      es <- flagSuspiciousTeamsPoints(et, game)
    } yield {
      game.copy(events = game.events + es)
    }
  }

  def allEventsIn(game: Game) = game.events.filter(onlyVerified)

  def lastEventOf(game: Game): Try[GameEvent] = Try(game.events.filter(onlyVerified).last)

  def last(nEvents: Int, game: Game): Either[String, SortedSet[GameEvent]] = {
    nEvents match {
      case n if n < 0 => Left("Number of events cannot be negative")
      case n if n > Try(game.events.size).getOrElse(0) => Left("Number of events cannot greater than the total events")
      case n => Right(game.events.filter(onlyVerified).takeRight(n))
    }
  }
}
