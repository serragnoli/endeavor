package endeavor.validation

import endeavor.model._

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

object GameEventsValidation {
  val onlyVerified: GameEvent => Boolean = _.verification == Verified

  def validateNoPointsScored(event: GameEvent): Either[String, GameEvent] =
    if (event.pointsScored > 0) Right(event)
    else Left(s"Event ${event.elapsedTime} not added due to invalid points scored: ${event.pointsScored}")

  def validateDuplicated(event: GameEvent, game: Game): Either[String, GameEvent] =
    if (game.events.contains(event)) Left(s"Event ${event.elapsedTime} not added as it is duplicated")
    else Right(event)

  def flagSuspiciousElapsedTime(event: GameEvent, game: Game): Either[String, GameEvent] = {
    Try(game.events.filter(onlyVerified).last) match {
      case Success(e) =>
        val absDiff = Math.abs((e.elapsedTime - event.elapsedTime)._1) seconds
        val maybePendingEvent = if (absDiff > game.threshold.action) event.copy(verification = ElapsedTimeUnreasonable) else event
        Right(maybePendingEvent)
      case Failure(_) => Right(event)
    }
  }

  def flagSuspiciousTeamsPoints(event: GameEvent, game: Game): Either[String, GameEvent] = {
    event match {
      case e if e.verification != Verified => Right(e)
      case e =>
        Try(game.events.filter(onlyVerified).last) match {
          case Success(le) if Math.abs(le.totalPointsTeam1 - e.totalPointsTeam1) > game.threshold.teamPoints => switchToUnreasonable(event)
          case Success(le) if Math.abs(le.totalPointsTeam2 - e.totalPointsTeam2) > game.threshold.teamPoints => switchToUnreasonable(event)
          case Success(_) | Failure(_) => Right(event)
        }
    }
  }

  private def switchToUnreasonable(e: => GameEvent): Either[String, GameEvent] = {
    val unreasonableEvent = e.copy(verification = TeamsPointsUnreasonable)
    Right(unreasonableEvent)
  }
}
