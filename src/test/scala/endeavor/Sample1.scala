package endeavor

import endeavor.model.{GameEvent, Team1, Team2}

import scala.concurrent.duration._
import scala.language.postfixOps

object Sample1 {
  val lastEvent = GameEvent(0, 598 seconds, 27, 29, Team1, 2)

  val allData: List[(Int, GameEvent)] = List(
    (0x801002, GameEvent(0, 16 seconds, 2, 0, Team1, 2)),
    (0xf81016, GameEvent(0, 31 seconds, 2, 2, Team2, 2)),
    (0x1d8102f, GameEvent(0, 59 seconds, 2, 5, Team2, 3)),
    (0x248202a, GameEvent(0, 73 seconds, 4, 5, Team1, 2)),
    (0x2e0203e, GameEvent(0, 92 seconds, 4, 7, Team2, 2)),
    (0x348204e, GameEvent(0, 105 seconds, 4, 9, Team2, 2)),
    (0x3b8384b, GameEvent(0, 119 seconds, 7, 9, Team1, 3)),
    (0x468385e, GameEvent(0, 141 seconds, 7, 11, Team2, 2)),
    (0x5304059, GameEvent(0, 166 seconds, 8, 11, Team1, 1)),
    (0x640406e, GameEvent(0, 200 seconds, 8, 13, Team2, 2)),
    (0x6d8506a, GameEvent(0, 219 seconds, 10, 13, Team1, 2)),
    (0x760606a, GameEvent(0, 236 seconds, 12, 13, Team1, 2)),
    (0x838607e, GameEvent(0, 263 seconds, 12, 15, Team2, 2)),
    (0x8e8707a, GameEvent(0, 285 seconds, 14, 15, Team1, 2)),
    (0x930708e, GameEvent(0, 294 seconds, 14, 17, Team2, 2)),
    (0x9f0709e, GameEvent(0, 318 seconds, 14, 19, Team2, 2)),
    (0xad070a5, GameEvent(0, 346 seconds, 14, 20, Team2, 1)),
    (0xb7880a2, GameEvent(0, 367 seconds, 16, 20, Team1, 2)),
    (0xbf880b6, GameEvent(0, 383 seconds, 16, 22, Team2, 2)),
    (0xc9080c6, GameEvent(0, 402 seconds, 16, 24, Team2, 2)),
    (0xd2090c2, GameEvent(0, 420 seconds, 18, 24, Team1, 2)),
    (0xdd090d6, GameEvent(0, 442 seconds, 18, 26, Team2, 2)),
    (0xed0a8d3, GameEvent(0, 474 seconds, 21, 26, Team1, 3)),
    (0xf98a8e6, GameEvent(0, 499 seconds, 21, 28, Team2, 2)),
    (0x10a8b8e2, GameEvent(0, 533 seconds, 23, 28, Team1, 2)),
    (0x1178b8ed, GameEvent(0, 559 seconds, 23, 29, Team2, 1)),
    (0x1228c8ea, GameEvent(0, 581 seconds, 25, 29, Team1, 2)),
    (0x12b0d8ea, lastEvent)
  )
}
