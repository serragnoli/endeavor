package endeavor

import endeavor.model._
import org.scalatest.{FunSuite, Matchers}

import scala.collection.immutable.SortedSet
import scala.concurrent.duration._
import scala.language.postfixOps

class GamesUtilForSample2Spec extends FunSuite with Matchers {

  private implicit val commonEventSettings: EventSettings = EventSettings(32, "0")

  private val threshold = Threshold(300 seconds, 50)
  private val emptyBasketballGame = Game.empty.copy(threshold = threshold)

  test("parsing event with all zeroes is invalid") {
    val allZeros = 0x0
    val event = GamesUtil.parseEvent(allZeros)

    val result = GamesUtil.addToGame(event.getOrElse(GameEvent.empty), emptyBasketballGame)

    result shouldBe Left("Event 0 seconds not added due to invalid points scored: 0")
  }

  test("parsing event with all numbers are 1") {
    val allOnes = 0xFFFFFFFF
    val event = GamesUtil.parseEvent(allOnes)

    val result = GamesUtil.addToGame(event.getOrElse(GameEvent.empty), emptyBasketballGame)

    result.getOrElse(Game.empty).events.last shouldBe GameEvent(1, 4095 seconds, 255, 255, Team2, 3, Verified)
  }

  test("querying the last existing events always returns a verified event") {
    val game = GamesUtil.addToGame(Sample2.tooSoon, emptyBasketballGame)
    val updatedGame = GamesUtil.addToGame(Sample2.insaneElapsedTime, game.getOrElse(Game.empty))

    val result = GamesUtil.lastEventOf(updatedGame.getOrElse(Game.empty))

    updatedGame.getOrElse(Game.empty).events should have size 2
    result.getOrElse(GameEvent.empty) shouldBe Sample2.tooSoon
  }

  test("querying the last N existing events always returns verified events") {
    val game = GamesUtil.addToGame(Sample2.tooLate, emptyBasketballGame)
    val updatedGame = GamesUtil.addToGame(Sample2.insaneElapsedTime, game.getOrElse(Game.empty))
    val gameOnlyTwoValidated = GamesUtil.addToGame(Sample2.tooSoon, updatedGame.getOrElse(Game.empty))

    val result = GamesUtil.last(2, gameOnlyTwoValidated.getOrElse(Game.empty))

    gameOnlyTwoValidated.getOrElse(Game.empty).events should have size 3
    result.getOrElse(SortedSet.empty[GameEvent]) should contain inOrderOnly(Sample2.tooLate, Sample2.tooSoon)
  }

  test("notify event with no points scored") {
    val event = GamesUtil.parseEvent(Sample2.noPointsScored1Tuple._1)

    val result = GamesUtil.addToGame(event.getOrElse(GameEvent.empty), Game.empty)

    result shouldBe Left(s"Event ${Sample2.noPointsScored1.elapsedTime} not added due to invalid points scored: ${Sample2.noPointsScored1.pointsScored}")
  }

  test("duplicated event doesn't get added twice") {
    val game = GamesUtil.addToGame(Sample2.duplicated, emptyBasketballGame)

    val result = GamesUtil.addToGame(Sample2.duplicated, game.getOrElse(Game.empty))

    result shouldBe Left(s"Event ${Sample2.duplicated.elapsedTime} not added as it is duplicated")
  }

  test("events out of order become ordered by elapsed time") {
    val game = GamesUtil.addToGame(Sample2.tooSoon, emptyBasketballGame)

    val result = GamesUtil.addToGame(Sample2.tooLate, game.getOrElse(Game.empty))

    result match {
      case Right(g) => g.events should contain inOrderOnly(Sample2.tooLate, Sample2.tooSoon)
      case Left(_) => fail("Should have received a valid game")
    }
  }

  test("elapsed time greater than threshold set in game is flagged as inconsistent") {
    val game = GamesUtil.addToGame(Sample2.first, emptyBasketballGame)

    val result = GamesUtil.addToGame(Sample2.insaneElapsedTime, game.getOrElse(Game.empty))

    result match {
      case Right(g) =>
        val lastEventRecorded = g.events.last
        lastEventRecorded.elapsedTime shouldBe Sample2.insaneElapsedTime.elapsedTime
        lastEventRecorded.verification shouldBe ElapsedTimeUnreasonable
      case Left(_) => fail("Should have received a valid game")
    }
  }

  test("points of team 1 greater than threshold set in game is flagged as inconsistent") {
    val game = GamesUtil.addToGame(Sample2.first, emptyBasketballGame)
    val insanePointsTeam1 = Sample2.tooSoon.copy(totalPointsTeam1 = threshold.teamPoints * 2)

    val result = GamesUtil.addToGame(insanePointsTeam1, game.getOrElse(Game.empty))

    result match {
      case Right(g) =>
        val lastEventRecorded = g.events.last
        lastEventRecorded.verification shouldBe TeamsPointsUnreasonable
      case Left(_) => fail("Should have received a valid game")
    }
  }

  test("points of team 2 greater than threshold set in game is flagged as inconsistent") {
    val game = GamesUtil.addToGame(Sample2.first, emptyBasketballGame)
    val insanePointsTeam2 = Sample2.tooSoon.copy(totalPointsTeam2 = threshold.teamPoints * 2)

    val result = GamesUtil.addToGame(insanePointsTeam2, game.getOrElse(Game.empty))

    result match {
      case Right(g) =>
        val lastEventRecorded = g.events.last
        lastEventRecorded.verification shouldBe TeamsPointsUnreasonable
      case Left(_) => fail("Should have received a valid game")
    }
  }

  test("query all events in a empty game returns empty") {
    GamesUtil.allEventsIn(Game.empty) shouldBe empty
  }

  test("query all events in the match so far returns all verified events") {
    val game = GamesUtil.addToGame(Sample2.first, emptyBasketballGame)
    val updatedGame = GamesUtil.addToGame(Sample2.insaneElapsedTime, game.getOrElse(Game.empty))
    val gameWithThreeEvents = GamesUtil.addToGame(Sample2.tooLate, updatedGame.getOrElse(Game.empty))

    val result = GamesUtil.allEventsIn(gameWithThreeEvents.getOrElse(Game.empty))

    result should contain inOrderOnly(Sample2.first, Sample2.tooLate)
  }

}
