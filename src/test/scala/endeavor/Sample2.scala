package endeavor

import endeavor.model.{GameEvent, Team1, Team2}

import scala.concurrent.duration._
import scala.language.postfixOps

object Sample2 {
  val first = GameEvent(0, 15 seconds, 2, 0, Team1, 2)
  val noPointsScored1 = GameEvent(0, 33 seconds, 2, 2, Team2, 0)
  val insaneElapsedTime = GameEvent(0, 1500 seconds, 232, 234, Team1, 3)
  val duplicated = GameEvent(0, 113 seconds, 4, 9, Team2, 2)
  val tooSoon = GameEvent(0, 195 seconds, 8, 13, Team2, 2)
  val tooLate = GameEvent(0, 168 seconds, 8, 11, Team1, 1)
  val noPointsScored2 = GameEvent(0, 234 seconds, 14, 13, Team2, 0)

  val noPointsScored1Tuple: (Int, GameEvent) = (0x1081014, noPointsScored1)
  val insaneElapsedTimeTuple: (Int, GameEvent) = (0x2ee74753, insaneElapsedTime)
  val duplicatedTuple: (Int, GameEvent) = (0x388204e, duplicated)
  val tooSoonTuple: (Int, GameEvent) = (0x618406e, tooSoon)
  val tooLateTuple: (Int, GameEvent) = (0x5404059, tooLate)

  val allData: List[(Int, GameEvent)] = List(
    (0x781002, first),
    (0xe01016, GameEvent(0, 28 seconds, 2, 2, Team2, 2)),
    noPointsScored1Tuple,
    (0x1e0102f, GameEvent(0, 60 seconds, 2, 5, Team2, 3)),
    (0x258202a, GameEvent(0, 75 seconds, 4, 5, Team1, 2)),
    (0x308203e, GameEvent(0, 97 seconds, 4, 7, Team2, 2)),
    (0x388204e, GameEvent(0, 113 seconds, 4, 9, Team2, 2)),
    duplicatedTuple,
    (0x3d0384b, GameEvent(0, 122 seconds, 7, 9, Team1, 3)),
    (0x478385e, GameEvent(0, 143 seconds, 7, 11, Team2, 2)),
    tooSoonTuple,
    tooLateTuple,
    (0x6b8506a, GameEvent(0, 215 seconds, 10, 13, Team1, 2)),
    (0x750706c, noPointsScored2),
    (0x7d8507e, GameEvent(0, 251 seconds, 10, 15, Team2, 2)),
    (0x938608e, GameEvent(0, 295 seconds, 12, 17, Team2, 2)),
    (0x8b8607a, GameEvent(0, 279 seconds, 12, 15, Team1, 2)),
    (0xa10609e, GameEvent(0, 322 seconds, 12, 19, Team2, 2)),
    (0xb8870a2, GameEvent(0, 369 seconds, 12, 20, Team1, 2)),
    (0xc4870b6, GameEvent(0, 393 seconds, 12, 22, Team2, 2)),
    (0xcc070c6, GameEvent(0, 408 seconds, 12, 24, Team2, 2)),
    insaneElapsedTimeTuple,
    (0xd5080c2, GameEvent(0, 426 seconds, 16, 24, Team1, 2)),
    (0xdf080d6, GameEvent(0, 446 seconds, 16, 26, Team2, 2)),
    (0xe4098d3, GameEvent(0, 456 seconds, 19, 26, Team1, 3)),
    (0xec098f6, GameEvent(0, 472 seconds, 19, 30, Team2, 2)),
    (0xfc8a8e2, GameEvent(0, 505 seconds, 21, 28, Team1, 2)),
    (0x10a8a8ed, GameEvent(0, 533 seconds, 21, 29, Team2, 1)),
    (0x1180b8ea, GameEvent(0, 560 seconds, 23, 29, Team1, 2)),
    (0x1218c8ea, GameEvent(0, 579 seconds, 25, 29, Team1, 2))
  )
}
