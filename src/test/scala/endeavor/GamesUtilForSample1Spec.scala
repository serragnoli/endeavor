package endeavor

import endeavor.Sample1.allData
import endeavor.model.{EventSettings, Game, GameEvent, Threshold}
import org.scalatest.TryValues._
import org.scalatest._

import scala.collection.immutable.SortedSet
import scala.concurrent.duration._
import scala.language.postfixOps

class GamesUtilForSample1Spec extends FunSuite with Matchers {

  private implicit val commonEventSettings: EventSettings = EventSettings(32, "0")

  private val threshold = Threshold(300 seconds, 50)
  private val emptyBasketballGame = Game.empty.copy(threshold = threshold)

  test("parsing all events in sample1") {
    allData.foreach { case (in, out) =>
      GamesUtil.parseEvent(in) shouldBe Right(out)
    }
  }

  test("querying the last existing event of the match") {
    val game = gameWithAllEvents

    val result = GamesUtil.lastEventOf(game)

    result.success.value shouldBe Sample1.lastEvent
  }

  test("querying the last event of an empty match") {
    val result = GamesUtil.lastEventOf(emptyBasketballGame)

    result.failure.exception should have message "empty tree"
  }

  test("query last N events of the match") {
    val game = gameWithAllEvents

    val result = GamesUtil.last(2, game)

    result.getOrElse(SortedSet.empty[GameEvent]).toList shouldBe Sample1.allData.takeRight(2).map(_._2)
  }

  test("query last Nth event greater than the number of events in game") {
    val game = gameWithAllEvents

    val oneTooManyTotalEvents = Sample1.allData.size + 1
    val result = GamesUtil.last(oneTooManyTotalEvents, game)

    result shouldBe Left("Number of events cannot greater than the total events")
  }

  test("query last Nth event negative number") {
    val game = gameWithAllEvents

    val result = GamesUtil.last(-1, game)

    result shouldBe Left("Number of events cannot be negative")
  }

  test("query last N events of an empty match") {
    val result = GamesUtil.last(2, emptyBasketballGame)

    result shouldBe Left("Number of events cannot greater than the total events")
  }

  private def gameWithAllEvents = {
    allData.foldLeft[Game](emptyBasketballGame) { (acc, sampleData) =>
      GamesUtil.parseEvent(sampleData._1)
        .flatMap(e => GamesUtil.addToGame(e, acc)) match {
        case Right(v) => v
        case Left(_) => acc
      }
    }
  }
}
